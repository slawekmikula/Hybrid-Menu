package de.kaesdingeling.hybridmenu.demo.page;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H5;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;

import de.kaesdingeling.hybridmenu.components.Notification;
import de.kaesdingeling.hybridmenu.components.NotificationCenter;
import de.kaesdingeling.hybridmenu.data.enums.NotificationPosition;
import de.kaesdingeling.hybridmenu.demo.DemoRouterLayout;

@Route(value = "notification/builder", layout = DemoRouterLayout.class)
public class NotificationBuilderPage extends VerticalLayout {
	private static final long serialVersionUID = 2225084532665233458L;

	public NotificationBuilderPage() {
		add(new H2("Notification Builder"), new H5("Here you can click a notification together"));
		
		HorizontalLayout outputPosition = new HorizontalLayout(new Button("Top", e -> VaadinSession.getCurrent().getAttribute(NotificationCenter.class).setNotificationPosition(NotificationPosition.TOP)), new Button("Bottom", e -> VaadinSession.getCurrent().getAttribute(NotificationCenter.class).setNotificationPosition(NotificationPosition.BOTTOM)));
		
		TextField caption = new TextField("Title");
		TextArea content = new TextArea("Content");
		
		ComboBox<VaadinIcon> icon = new ComboBox<VaadinIcon>("Icon");
		
		icon.setItemLabelGenerator((i) -> i.name());
		icon.setItems(VaadinIcon.values());
		
		NumberField displayTime = new NumberField();

		displayTime.setTitle("Display time (ms)");
		displayTime.setHasControls(true);
		displayTime.setStep(1);
		displayTime.setMin(1000);
		displayTime.setMax(10000);
		displayTime.setValue(5000.0);
		
		Checkbox closeable = new Checkbox("Closeable");
		Checkbox autoRemove = new Checkbox("Autoremove");
		Checkbox makeAsReaded = new Checkbox("Make as readed");
		Checkbox showDescriptionOnPopup = new Checkbox("Show description on popup");
		
		showDescriptionOnPopup.setValue(true);
		
		NumberField autoRemoveTime = new NumberField();

		autoRemoveTime.setTitle("Autoremove display offset delay (ms)");
		autoRemoveTime.setHasControls(true);
		autoRemoveTime.setStep(1);
		autoRemoveTime.setMin(0);
		autoRemoveTime.setMax(5000);
		autoRemoveTime.setValue(0.0);
		
		Button execute = new Button("Execute", VaadinIcon.CODE.create());
		
		//execute.setDisableOnClick(true);
		execute.addClickListener(e -> {
			NotificationCenter notificationCenter = VaadinSession.getCurrent().getAttribute(NotificationCenter.class);
			if (notificationCenter.queueSize() < 10) {
				Notification notification = Notification.get()
						.withTitle(caption.getValue())
						.withContent(content.getValue())
						.withIcon(icon.getValue())
						.withDisplayTime(displayTime.getValue().longValue());
				
				if (closeable.getValue()) {
					notification.withCloseable();
				}
				
				if (makeAsReaded.getValue()) {
					notification.makeAsReaded();
				}
				
				if (autoRemove.getValue()) {
					if (autoRemoveTime.getValue().longValue() > 0) {
						notification.withAutoRemove(autoRemoveTime.getValue().longValue());
					} else {
						notification.withAutoRemove();
					}
				}
				
				notificationCenter.add(notification, showDescriptionOnPopup.getValue());
			}
		});
		
		add(outputPosition, caption, content, icon, displayTime, closeable, makeAsReaded, autoRemove, showDescriptionOnPopup, autoRemoveTime, execute);
	}
}