package de.kaesdingeling.hybridmenu.data;

import com.vaadin.flow.component.icon.VaadinIcon;

import de.kaesdingeling.hybridmenu.data.enums.ETheme;
import de.kaesdingeling.hybridmenu.data.enums.NotificationPosition;

/**
 * 
 * @created 02.10.2020 - 14:31:15
 * @author KaesDingeling
 * @version 0.1
 */
public class MenuConfig {
	
	private ETheme theme = ETheme.Lumo;
	
	private VaadinIcon subMenuIcon = VaadinIcon.ANGLE_UP;

	private VaadinIcon notificationButtonIcon = VaadinIcon.BELL;
	private VaadinIcon notificationButtonEmptyIcon = VaadinIcon.BELL_O;
	private VaadinIcon notificationCenterCloseIcon = VaadinIcon.ANGLE_RIGHT;
	private VaadinIcon notificationRemoveIcon = VaadinIcon.CLOSE;

	private NotificationPosition notificationPosition = NotificationPosition.BOTTOM;

	private long notificationDisplayTime = 5000;

	private VaadinIcon breadcrumbSeperatorIcon = VaadinIcon.ANGLE_RIGHT;
	private boolean breadcrumbs = true;
	
	public static int notificationQueueMax = 200;
	
	/**
	 * 
	 * @param theme
	 * @Created 02.10.2020 - 14:31:59
	 * @author KaesDingeling
	 */
	public void setTheme(final ETheme theme) {
		this.theme = theme;
	}
	
	/**
	 * 
	 * @return
	 * @Created 02.10.2020 - 14:31:41
	 * @author KaesDingeling
	 */
	public ETheme getTheme() {
		return theme;
	}

	/**
	 * @return the subMenuIcon
	 */
	public VaadinIcon getSubMenuIcon() {
		return subMenuIcon;
	}

	/**
	 * @param subMenuIcon the subMenuIcon to set
	 */
	public void setSubMenuIcon(VaadinIcon subMenuIcon) {
		this.subMenuIcon = subMenuIcon;
	}

	/**
	 * @return the notificationButtonIcon
	 */
	public VaadinIcon getNotificationButtonIcon() {
		return notificationButtonIcon;
	}

	/**
	 * @param notificationButtonIcon the notificationButtonIcon to set
	 */
	public void setNotificationButtonIcon(VaadinIcon notificationButtonIcon) {
		this.notificationButtonIcon = notificationButtonIcon;
	}

	/**
	 * @return the notificationButtonEmptyIcon
	 */
	public VaadinIcon getNotificationButtonEmptyIcon() {
		return notificationButtonEmptyIcon;
	}

	/**
	 * @param notificationButtonEmptyIcon the notificationButtonEmptyIcon to set
	 */
	public void setNotificationButtonEmptyIcon(VaadinIcon notificationButtonEmptyIcon) {
		this.notificationButtonEmptyIcon = notificationButtonEmptyIcon;
	}

	/**
	 * @return the notificationCenterCloseIcon
	 */
	public VaadinIcon getNotificationCenterCloseIcon() {
		return notificationCenterCloseIcon;
	}

	/**
	 * @param notificationCenterCloseIcon the notificationCenterCloseIcon to set
	 */
	public void setNotificationCenterCloseIcon(VaadinIcon notificationCenterCloseIcon) {
		this.notificationCenterCloseIcon = notificationCenterCloseIcon;
	}

	/**
	 * @return the notificationRemoveIcon
	 */
	public VaadinIcon getNotificationRemoveIcon() {
		return notificationRemoveIcon;
	}

	/**
	 * @param notificationRemoveIcon the notificationRemoveIcon to set
	 */
	public void setNotificationRemoveIcon(VaadinIcon notificationRemoveIcon) {
		this.notificationRemoveIcon = notificationRemoveIcon;
	}

	/**
	 * @return the notificationPosition
	 */
	public NotificationPosition getNotificationPosition() {
		return notificationPosition;
	}

	/**
	 * @param notificationPosition the notificationPosition to set
	 */
	public void setNotificationPosition(NotificationPosition notificationPosition) {
		this.notificationPosition = notificationPosition;
	}

	/**
	 * @return the notificationDisplayTime
	 */
	public long getNotificationDisplayTime() {
		return notificationDisplayTime;
	}

	/**
	 * @param notificationDisplayTime the notificationDisplayTime to set
	 */
	public void setNotificationDisplayTime(long notificationDisplayTime) {
		this.notificationDisplayTime = notificationDisplayTime;
	}

	/**
	 * @return the breadcrumbSeperatorIcon
	 */
	public VaadinIcon getBreadcrumbSeperatorIcon() {
		return breadcrumbSeperatorIcon;
	}

	/**
	 * @param breadcrumbSeperatorIcon the breadcrumbSeperatorIcon to set
	 */
	public void setBreadcrumbSeperatorIcon(VaadinIcon breadcrumbSeperatorIcon) {
		this.breadcrumbSeperatorIcon = breadcrumbSeperatorIcon;
	}

	/**
	 * @return the breadcrumbs
	 */
	public boolean isBreadcrumbs() {
		return breadcrumbs;
	}

	/**
	 * @param breadcrumbs the breadcrumbs to set
	 */
	public void setBreadcrumbs(boolean breadcrumbs) {
		this.breadcrumbs = breadcrumbs;
	}
}