package de.kaesdingeling.hybridmenu.data.enums;

/**
 * 
 * @created 02.10.2020 - 14:30:59
 * @author KaesDingeling
 * @version 0.1
 */
public enum ETheme {
	
	Lumo("./HybridMenu/Styles/Lumo.css"),
	Material("./HybridMenu/Styles/Material.css"),
	None(null);
	
	private final String path;
	
	/**
	 * 
	 * @param path
	 */
	private ETheme(final String path) {
		this.path = path;
	}
	
	/**
	 * 
	 * @return
	 * @Created 02.10.2020 - 14:32:53
	 * @author KaesDingeling
	 */
	public String getPath() {
		return path;
	}
}