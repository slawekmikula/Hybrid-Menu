package de.kaesdingeling.hybridmenu.components;

import java.util.List;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.dom.Element;

import de.kaesdingeling.hybridmenu.data.interfaces.MenuComponent;

/**
 * Only for the left menu
 * 
 * @created 01.09.2020 - 22:43:36
 * @author KaesDingeling
 * @version 0.1
 */
@Tag("label")
@SuppressWarnings("hiding")
public class HMLabel extends Div implements MenuComponent<HMLabel> {
	private static final long serialVersionUID = -1030806788859361123L;

	private Element caption = null;
	private Element icon = null;
	
	/**
	 * 
	 * @return
	 * @Created 01.09.2020 - 22:43:41
	 * @author KaesDingeling
	 */
	public static HMLabel get() {
		return new HMLabel();
	}
	
	/**
	 * 
	 * @param caption
	 * @return
	 * @Created 01.09.2020 - 22:43:32
	 * @author KaesDingeling
	 */
	public HMLabel withCaption(String caption) {
		if (this.caption != null) {
			getElement().removeChild(this.caption);
		}
		
		if (caption != null) {
			this.caption = getElement().appendChild(new Html("<p>" + caption + "</p>").getElement());
		}
		
		return this;
	}
	
	/**
	 * 
	 * @param icon
	 * @return
	 * @Created 01.09.2020 - 22:43:29
	 * @author KaesDingeling
	 */
	public HMLabel withIcon(VaadinIcon icon) {
		return withIcon(icon.create());
	}
	
	/**
	 * 
	 * @param icon
	 * @return
	 * @Created 01.09.2020 - 22:43:28
	 * @author KaesDingeling
	 */
	public HMLabel withIcon(Component icon) {
		if (this.icon != null) {
			getElement().removeChild(this.icon);
		}
		
		if (icon != null) {
			this.icon = getElement().insertChild(0, icon.getElement());
		}
		
		return this;
	}
	
	/**
	 * 
	 * @param listener
	 * @return
	 * @Created 01.09.2020 - 22:43:25
	 * @author KaesDingeling
	 */
	public HMLabel withClickListener(ComponentEventListener<ClickEvent<Div>> listener) {
		addClickListener(listener);
		
		return this;
	}
	
	@Override
	public <MenuComponent extends Component> MenuComponent add(MenuComponent c) {
		return null;
	}

	@Override
	public <MenuComponent extends Component> MenuComponent addAsFirst(MenuComponent c) {
		return null;
	}

	@Override
	public <MenuComponent extends Component> MenuComponent addAt(MenuComponent c, int index) {
		return null;
	}

	@Override
	public int count() {
		return 0;
	}

	@Override
	public <MenuComponent extends Component> HMLabel remove(MenuComponent c) {
		return null;
	}

	@Override
	public List<MenuComponent<?>> getList() {
		return null;
	}
}