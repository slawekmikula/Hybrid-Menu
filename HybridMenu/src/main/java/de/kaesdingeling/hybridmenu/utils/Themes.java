package de.kaesdingeling.hybridmenu.utils;

/**
 * 
 * @created 02.10.2020 - 13:07:14
 * @author KaesDingeling
 * @version 0.1
 */
public class Themes {
	
	public static final String TOP_MENU = "hybridmenu-top";
	public static final String LEFT_MENU = "hybridmenu-left";
}